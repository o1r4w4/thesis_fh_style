`document.tex` is the root tex file

all included tex files are located in the `tex` folder

all included figures are located in the `fig` folder

the bibliography entries are stored in `lit.bib`